import { Link } from "react-router-dom"
import { useCartContext } from "../../context/CartContext"


const CartCountainer = () => {
    const { 
        cartList, 
        vaciarCarrito, 
        precioTotal,
        eliminarProducto 
    } = useCartContext()
    
    console.log(cartList)
    
    return (
        cartList.length === 0 ? 
            <center>
                <h2>No hay productos</h2>
                <Link to='/'> ⬅ Ir a ver productos</Link>
            </center>
        :
            <div>
                    { cartList.map(product => (
                        <li key={product.id}>
                            <img src={product.foto} alt='imagen' className="w-25" />
                            Nombre: {product.name} - Cantidad: {product.quantity}
                            <button className="btn btn-danger" onClick={()=> eliminarProducto(product.id)}> {' '} X {' '} </button>
                        </li>
                    ))}
                    <h3>Precio Total: {precioTotal()}</h3>
                    <button onClick={vaciarCarrito}>Vaciar CArrito</button>
            </div>
    )
}

export default CartCountainer