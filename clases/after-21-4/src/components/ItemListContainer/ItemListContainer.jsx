import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore'

import { mockFetch } from "../../utils/mockFetch"
import ItemList from "../ItemList/ItemList"
import Loading from "../Loading/Loading"
// import { Filter } from "../RenderProps/Filter"
// import { TextComponent } from "../../../clases/frontend-comision39630-myj-m/clases/clase12-rendering/ComponenteEjemplosCondicionales"

const ItemListContainer = () => {
    const [productos, setProductos] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [ meGusta, setMeGusta ]   = useState(true)
    const {cid} = useParams()
   
    useEffect(()=>{
        const db = getFirestore()        
        const queryCollection = collection(db, 'productos') // 2 argumento
        if (cid) {
            const queryFilter = query(queryCollection,where('categoria', '==', cid)) // filtro    
            
            getDocs(queryFilter)
                .then(resp => setProductos( resp.docs.map(producto => ({ id: producto.id, ...producto.data() }) ) ))
                .catch(err => console.log(err))
                .finally(()=> setIsLoading(false))        
        } else {              
            
            getDocs(queryCollection)
                .then(resp => setProductos( resp.docs.map(producto => ({ id: producto.id, ...producto.data() }) ) ))
                .catch(err => console.log(err))
                .finally(()=> setIsLoading(false))       
                       
        }
    }, [cid])    
    
    console.log(productos)
    // 
    const handleMeGusta= () => {
        setMeGusta(!meGusta)
    }

    const handleAgregarProducto = () => {
        setProductos([
            ...productos, 
            {id: productos.length+1 , name: 'Nuevo Producto', categoria: '', price: 3500, stock: 100}
        ])
    }


   console.log('ItemListContianer')
    return (
        <>
            <button className="btn btn-success rounded-pill" onClick={handleMeGusta}>Me gusta</button>
            <button className="btn btn-success" onClick={handleAgregarProducto}>Agregar Producto</button>
            { isLoading ?
                    <Loading />  
                :
                    <ItemList 
                        productos = {productos}
                    />
            
            }       
              
        </>
    )
}

export default ItemListContainer


//      Por otro lado, Render Props es un patrón que implica pasar una función como una prop al componente hijo, 
//      que luego puede ser utilizada por el componente para renderizar su contenido. 
//      En otras palabras, un componente con Render Props proporciona una 
//      función a sus hijos que les permite personalizar su contenido.



// const hanldePoductsFiltered =  ( { filterState, handleFilterChange }) => (
        
        
    //     <div>
    //         <h2>Buscar Producto</h2>
    //         <h2>{filterState}</h2>
            
    //         <input type="text" value={filterState} onChange={handleFilterChange} />
            
    //         <ItemList 
    //             productos = {
    //                 filterState === '' ?
    //                     productos
    //                 :
    //                     productos.filter( producto => producto.name.toLowerCase().includes(filterState.toLowerCase()) )
    //             }
    //         />
    //     </div>
        
    // )
