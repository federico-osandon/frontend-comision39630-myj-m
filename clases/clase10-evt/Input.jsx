import {useState, useEffect} from 'react'
// import './input.css'


export const Input = () => {   

    const inputHandler = (event)=>{
        // event.preventDefault() // cancelar onKey down
        if ( ['a','e','i','o','u'].includes(event.key) ) {
            event.preventDefault()            
        }
        
        console.log(event.key)   

        // console.log(event.nativeEvent)   
          
    }
    // addEventListener('keydown', inputHandler')
    return (
        <div className="box" >
            <div className="border border-5 border-warning  m-3" >
                <input 
                    className="m-5 " 
                    onKeyDown={ inputHandler } 
                    // onClick = {  inputHandler } 
                    type="text" 
                    name="nombre" 
                    id="i"
                />
            </div>
        </div>
    )
}
