import { useCartContext } from "../../context/CartContext"


const CartCountainer = () => {
    const { cartList, vaciarCarrito } = useCartContext()
    console.log(cartList)
    return (
      <div>
            { cartList.map(product => (
                <li key={product.id}>
                    <img src={product.foto} alt='imagen' className="w-25" />
                    Nombre: {product.name} - Cantidad: {product.quantity}
                    <button> X </button>
                </li>
            ))}
            <button onClick={vaciarCarrito}>Vaciar CArrito</button>
      </div>
    )
}

export default CartCountainer