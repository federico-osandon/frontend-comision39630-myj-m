import { useCartContext } from "../../context/CartContext"
import { ItemCount } from "../ItemCount/ItemCount"



const ItemDetail = ({product}) => {

	const { agregarAlCart } = useCartContext()

	const onAdd = (cantidad) => {  // { product, cantidad:  }
		console.log(cantidad)
		agregarAlCart( { ...product, quantity: cantidad } )
	} 
//   console.log(cartList)
    return (
      <div className="row">
	  	<div className="col-6">
			<img src={product.foto} className="w-25" />
			<h3>Nombre: {product.name}</h3>
			<h4>Precio: {product.price}</h4>
			<h4>Categoria: {product.categoria}</h4>
		</div>
		<div className="col-6">
		{/* condition */}
			<ItemCount inital={1} stock={5} onAdd={onAdd} />
		</div>
      </div>
    )
}

export default ItemDetail