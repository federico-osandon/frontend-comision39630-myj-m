import { useRef, useState } from 'react'

import Titulo from './components/Titulo/Titulo'
import { Formulario } from './components/Formulario/Formulario'
import './App.css'


function App() { // componente funcional  // c/evento - cambio de valor de estado(setContador)  ejecuta de nuevo el componente= re render 
    // estado
    const [contador, setContador] = useState(0) // [ ] <- // no if - no for no nada
    
    const titulo = 'App'
    const subtitulo = 'App'
    const saludar = () => console.log('SAludando')
// contador __________________________________
    // let contador = 0 
    const handleContador = () => {
        // contador = contador + 1 // contador++ // contador += 1
        // console.log(contador)
        setContador(contador + 1)
    }

// __________________________________
    

    return (
        <div className='App' >
            {/* // Titulo({titutlo: ''}) */}            
            <Titulo titulo={titulo} subtitulo={subtitulo }/>                
                           
            <Formulario saludar={saludar} />

            <div>
                <h2>Contador: {contador}</h2>
                <button onClick={handleContador}>Sumar</button>
            </div>         
            
        </div>
    )
}

export default App
