import { useRef, useState } from 'react'

import { ItemCount } from './components/ItemCount/ItemCount'
import Titulo from './components/Titulo/Titulo'
import { Formulario } from './components/Formulario/Formulario'
import './App.css'
// promesas -> saber usar -> mock (simulación)
// Promesa -> 3 estados: pendiente - aceptado y rechazado
let task = (numero1, numero2) =>{
    return new Promise( (resolve, reject)=>{ // 12/6
        // acciones
        if (numero2===0) {
            reject('no se puede dividir 0')
            
        } else {
            resolve(numero1/numero2)
            
        }
    } )
}


task()
    // .then(resp => {
    //     // throw new Error('Error forzado')
    //     // console.log(resp)
    //     return resp * 5
    // // }, err=>console.log(err))
    // })
    .then(resp => resp*5)
    .then(resp => console.log(resp))
    .catch(err => console.log(err))
    .finally(()=>console.log('Siempre y al último'))

function App() { 
    const divRef = useRef(null)
    // console.log(divRef)

    const handleClick = () => {
        divRef.current.innerHTML = 'nuevo contenido'
    }
    console.log('Componente App')
    return(
        <div className='App'>
            <div ref={divRef}>
                Contenido original
            </div>
            <button onClick={handleClick}>Cambiar valor</button>
            <ItemCount />
        </div>
)}

export default App


// render 
// 1 ver -> montaje
// sgtes re render 
// última -> dismounting

// que produce un re render : 
    // cambios en un props
    // cambios en un estados
    // - evento