import { useEffect, useState } from "react"
import { mockFetch } from "../../src/utils/mockFetch"
import ItemList from "../ItemList/ItemList"

const ItemListContainer = ({greeting='saludando'}) => {
    const [productos, setProductos] = useState([])
    

    let usuarios= [
        {nombre: "Federico", apellido: "Osandón"},
        {nombre: "Federico", apellido: "Osandón"}                
    ]

    // fetch
    // fetch(url,{
    //     method: 'POST',// verbo por defecto: GET 
    //     headers: {
    //         'Content-Type': 'apllication/json'
    //     },
    //     body: JSON.stringify(usuarios)
        
    // })

    // fetch con async await
    // useEffect(()=>{
    //     let url = "https://pokeapi.co/api/v2/ability/?limit=20&offset=20" 

    //     const query = async () => {
    //         let resp = await fetch(url) 
    //         let respParse = await resp.json()            // setPokes(respParse) 
    //     }
    //     query()
    // }, [])

    // cors
    useEffect(()=>{
        let url = "https://google.com" 

        const query = async () => {
            let resp = await fetch(url) 
            console.log(resp)            // setPokes(respParse) 
        }
        query()
    }, [])
    
    useEffect(()=>{
        mockFetch()        
            .then(resp => setProductos(resp))
            .catch(err => console.log(err))
            .finally(()=>console.log('Siempre y al último'))        
    }, [])
   
    return (
        <>
            <div>
                ItemListContainer { greeting }
            </div>
            <div>
            { 
                productos.length !== 0 ? 
                   
                    <ItemList productos={productos} />
                    
                :
                    <h2>Cargando...</h2>
            }

        </div>

        </>
    )
}

export default ItemListContainer