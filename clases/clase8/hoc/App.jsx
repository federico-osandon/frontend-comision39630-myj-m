import NavBar from './components/NavBar/NavBar'
// import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import ItemListContainer from './components/ItemListContainer/ItemListContainer'
import { ItemCount } from './components/ItemCount/ItemCount'
import { FormContainer } from './components/FormContainer/FormContainer'

const Box = ({title, children}) => {
    return (
        <div className='box'>
            <h2>{title}</h2>
            <div className='container'>
                {children} 
            </div>
        </div>
    )
}

function App() {    
    
    return(
        <div className='App'>
            {/* <NavBar /> */}
            <ItemListContainer greeting='estoy saludando' />
            {/* <Box title='Mi Caja'>
                <p>Contenido Personalizado</p>
                <button>Botón</button> 
                <ItemCount />
            </Box> */}
            <FormContainer />
        </div>
)}

export default App


// render 
// 1 ver -> montaje
// sgtes re render 
// última -> dismounting

// que produce un re render : 
    // cambios en un props
    // cambios en un estados
    // - evento