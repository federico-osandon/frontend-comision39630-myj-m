import { BrowserRouter, Routes, Route, Navigate} from 'react-router-dom'
import NavBar from './components/NavBar/NavBar'
import ItemListContainer from './components/ItemListContainer/ItemListContainer'
import ItemDetailContainer from './components/ItemDetailContainer/ItemDetailContainer'
import CartCountainer from './components/CartContainer/CartCountainer'

import 'bootstrap/dist/css/bootstrap.min.css'

const Box = ({title, children}) => {
    return (
        <div className='box'>
            <h2>{title}</h2>
            <div className='container'>
                {children} 
            </div>
        </div>
    )
}

function App() {    
    
    return(
        <BrowserRouter>
            <div className='App'>
                <NavBar />
                <Routes>
                    <Route path='/' element={<ItemListContainer greeting='estoy saludando' />} />
                    <Route path='/categoria/:cid' element={<ItemListContainer greeting='estoy saludando' />} />

                    <Route path='/detail/:pid' element={<ItemDetailContainer /> } />
                    <Route path='/cart' element={<CartCountainer />} />  
                    {/* <Route path='//404notfound' element={<404NotFoun />} />   */}

                    <Route path='*' element={<Navigate to='/' />} />           
                    {/* <Route path='*' element={<Navigate to='/404notfound' />} />            */}
                </Routes>
                {/* <Footer />             */}
            </div>
        </BrowserRouter>
)}

export default App

