import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import ItemDetail from "../ItemDetail/ItemDetail"
import { mockFetch } from "../../utils/mockFetch"

const ItemDetailContainer = () => {
    const [product, setProduct] = useState({})
    const {pid} = useParams()

    
    // llamada a la api - mock -> un producto

    useEffect(()=>{
        mockFetch(pid) // ojo con el tipo de dato
        .then(resp => setProduct(resp))
        .catch((err)=> console.log(err))
    }, [])

    return (
        <ItemDetail product={product} />
    )
}

export default ItemDetailContainer