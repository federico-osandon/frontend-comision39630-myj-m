import { addDoc, collection, doc, getFirestore, updateDoc, writeBatch } from "firebase/firestore"
import { Link } from "react-router-dom"
import { useCartContext } from "../../context/CartContext"
import { useState } from "react"


const CartCountainer = () => {
    const [ id, setId] = useState(null)
    const [formData, setFormData] = useState({
        name: '',
        phone: '',
        email: ''
    })
    const { 
        cartList, 
        vaciarCarrito, 
        precioTotal,
        eliminarProducto 
    } = useCartContext()
    
    console.log(cartList)

    const handleSubmit = (event) => {
        event.preventDefault()

        const order={
            buyer: formData, // crear furmulario 
            items: cartList.map(({id, name, price})=> ({id, name, price})), // reduce campos
            total: precioTotal() // precio total de la compra
        }

        const db = getFirestore()
        
        const queryCollection = collection(db, 'orders')
        // agregar
        addDoc(queryCollection, order)
        .then(resp =>setId(resp.id))
        .catch(err => console.log(err))
        .finally(()=>{
                console.log('termino la promesa')
                // tengo función para borrar todo
                vaciarCarrito()
        })
        
        
        
        
        // Actualizar 

        // const queryDoc = doc(db, 'productos', 'ecQLLyKTPcgZ5fA4O9QP')

        // updateDoc(queryDoc, {
        //     stock: 99,
        //     isActive: false
        // })
        // .finally(()=> console.log('Terminó de actualizar'))
            


        // realizar multiples operaciones
        // const queryCollection = collection(db, 'orders')

        // const queryDoc1 = doc(queryCollection)
        // const queryDoc2 = doc(queryCollection)
        // const queryDoc3 = doc(queryCollection, 'UPQLhXUDeM67Erjbxik6')

        // const batch = writeBatch(db)

        // batch.set(queryDoc1, {buyer: 'Nombre 1', items: [], total: 1500})
        // batch.set(queryDoc2,  {buyer: 'Nombre 2', items: [], total: 3500})
        // batch.update(queryDoc3,  {buyer: 'Producot 3', items: [], total: 5500})
        

        // batch.commit()




        // console.log('enivando orden: ',order)
    }

    console.log(formData)

    const handleOnChange = (event)=> {
        console.log(event.target.name)
        console.log(event.target.value)
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }
    
    return (
        <>
        {id && <h2>El id de la orden de la compra es: {id}</h2>}
        {cartList.length === 0 ? 
            <center>
                <h2>No hay productos</h2>
                <Link to='/'> ⬅ Ir a ver productos</Link>
            </center>
        :
            <div>
                    { cartList.map(product => (
                        <li key={product.id}>
                            <img src={product.foto} alt='imagen' className="w-25" />
                            Nombre: {product.name} - Cantidad: {product.quantity}
                            <button className="btn btn-danger" onClick={()=> eliminarProducto(product.id)}> {' '} X {' '} </button>
                        </li>
                    ))}
                    <h3>Precio Total: {precioTotal()}</h3>
                    <button onClick={vaciarCarrito}>Vaciar CArrito</button>

                    <form  onSubmit={handleSubmit}>
                        <input                         
                            type="text"
                            name="name"
                            placeholder="ingrese el nomber"
                            onChange={handleOnChange}
                            value={formData.name}
                        />
                        <input                         
                            type="text"
                            name="phone"
                            placeholder="ingrese el teléfono"
                            onChange={handleOnChange}
                            value={formData.phone}
                        />
                        <input                         
                            type="text"
                            name="email"
                            placeholder="ingrese el mail"
                            onChange={handleOnChange}
                            value={formData.email}
                        />
                        <input                         
                            type="text"
                            name="repetirMail"
                            placeholder="repetir el mail "
                            onChange={()=>{}}
                            // value={''}
                        />
                        <button >Generar Orden</button>
                    </form>
            </div>
        }
        </>
    )
}

export default CartCountainer