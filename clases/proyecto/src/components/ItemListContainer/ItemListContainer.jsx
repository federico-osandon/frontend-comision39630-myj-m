import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore'

import ItemList from "../ItemList/ItemList.jsx"
import Loading from "../Loading/Loading"

const ItemListContainer = () => {
    const [productos, setProductos] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
    const {cid} = useParams()
   
    /* This code block is using the `useEffect` hook to fetch data from a Firestore database. It
    first initializes the Firestore instance and creates a query to the "productos" collection. If
    a `cid` parameter is present, it adds a filter to the query to only retrieve documents where
    the "categoria" field matches the `cid` value. It then uses the `getDocs` function to execute
    the query and retrieve the documents, and maps over the resulting `resp.docs` array to extract
    the document data and add an `id` field to each object. Finally, it sets the `productos` state
    with the resulting array and sets the `isLoading` state to `false`. The `cid` parameter is
    included in the dependency array of the `useEffect` hook, so the effect will re-run whenever
    the `cid` value changes. */

    useEffect(()=>{
    const db = getFirestore()        
    const queryCollection = collection(db, 'productos') // 2 argumento
    const queryFilter = cid ? query(queryCollection,where('categoria', '==', cid))   : queryCollection

    getDocs(queryFilter)
    .then(resp => setProductos( resp.docs.map(producto => ({ id: producto.id, ...producto.data() }) ) ))
    .catch(err => console.log(err))
    .finally(()=> setIsLoading(false))        
       
    }, [cid])   

   
    return (
        <>
            { isLoading 
                ?
                    <Loading />  
                :
                    <ItemList 
                        productos = {productos}
                    />
            
            }       
              
        </>
    )
}

export default ItemListContainer

// codigo repetido eliminar
// eliminar todos los console.log, ni errores ni advertencias
// si no lo necesitamos que no esté
// ni variables ni importaciones declaradas nunca usadas
// no código comentado
// código bin identado
// no mezclar idiomas en el cód. 
// modularizar


//      Por otro lado, Render Props es un patrón que implica pasar una función como una prop al componente hijo, 
//      que luego puede ser utilizada por el componente para renderizar su contenido. 
//      En otras palabras, un componente con Render Props proporciona una 
//      función a sus hijos que les permite personalizar su contenido.


